import React from 'react';
import './styles.css'
import {Link} from 'react-icons/fi';
import { FiArrowLeft } from 'react-icons/fi';
import heroesImg from '../../assets/heroesImg.jpg';

export default function Register(){
    return (
        <div className="register-container">
            <div className="content">
                <section>
                    <img src={heroesImg} alt="Be the hero"/>
                    <h1>Cadastro</h1>
                    <p>Faça seu cadastro, entre na platarforma e ajude pessoas e
                         ajude pessoas a encontrarem os casos de sua ONG</p>
                         <Link className="back-link" to="/register">
                            <FiArrowLeft size={16} color="#E02041"/>
                            Não é cadastrado.
                            </Link>

                </section>
                <form>
                    <input placeholder="Nome da ONG" />
                    <input type="email" placeholder="E-mail"/>
                    <input placeholder="WhatsApp" />

                    <div className="input-group">
                        <input placeholder="Cidade" />
                        <input placeholder="UF" style={{width: 80}} />
                    </div>
                    <button className="button" type="submit">Cadastro</button>

                </form>

            </div>

        </div>
    )
}