import React from 'react';
import heroesImg from '../../assets/heroesImg.jpg';
import './styles.css'
import {Link} from 'react-router-dom';
import { FiLogIn } from 'react-icons/fi';


export default function Logon (){
  return (
    <div className="logan-container">
      <section className="form">
        <form>
          <h1>Faça seu logon</h1>
          <input placeholder="Sua ID"/>
          <button className="button" type="submit">ENTRAR</button>

          <Link className="back-link" to="/register">
            <FiLogIn size={16} color="#E020401"/>
            Não é cadastrado.
          </Link>
        </form>
      </section>
      <img src={heroesImg} alt="Heroes" />

    </div>
  )
}