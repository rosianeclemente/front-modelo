
import React from 'react';
import './styles.css';
import {Link} from 'react-icons/fi'
import { FiArrowLeft } from 'react-icons/fi'



export default function NewIncident(){
    return (
        <div className="new-incident">
            <div className="content">
                <section>
                    
                    <h1>Cadastra novo caso</h1>
                    <p>Descreva o caso detalhadamente para encontrar um heroi para resolver isso</p>
                         <Link className="back-link" to="/profile">
                            <FiArrowLeft size={16} color="#E02041"/>
                            Voltar para home.
                            </Link>

                </section>
                <form>
                    <input placeholder="Titulo" />
                    <input type="email" placeholder="E-mail"/>
                    <input placeholder="WhatsApp" />

                    <div className="input-group">
                        <input placeholder="Cidade" />
                        <input placeholder="UF" style={{width: 80}} />
                    </div>
                    <button className="button" type="submit">Cadastro</button>

                </form>

            </div>

        </div>
    )
}